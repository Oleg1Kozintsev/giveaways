import Vue from 'vue'
import axios from 'axios'

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  data: {
    errors: [],
    url: null,
    profilePicUrl: null,
    username: null,
    fullname: null,
    pooling: true,
    code: null
  },
  computed: {
    postId: function () {
      const str = document.getElementById('postId').value;
      const id = parseInt(str);
      console.log("Get post id : " + str + " Parse int: " + id);
      return id;
    }
  },
  mounted: function () {
    this.startPolling();
  },
  methods: {
    startPolling: function () {
      let self = this;
      axios.get('/info/' + this.postId)
        .then(function (response) {
          // handle success
          //console.log(response);
          if (response.data.status === 'found') {
            self.pooling = false;
            self.username = response.data.username;
            self.fullname = response.data.fullname;
            self.profilePicUrl = response.data.profilePicUrl;
            self.code = response.data.code;
            self.url = `/post/${self.postId}`;
            // показать форму
          }
        })
        .catch(function (error) {
          // handle error
          //console.log(error);
        })
        .then(function () {
          // always executed
          //console.log('always executed');
          if (self.pooling === true) {
            console.log('pooling');
            setTimeout(() => {
              self.startPolling();
            }, 3000);
          }
        });
    },
  }
});
