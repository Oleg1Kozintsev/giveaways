<?php

namespace App\Http\Controllers;

use App\Account;
use App\NewPost;
use App\Post;
use App\Winner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private const PostBaseUrl = 'https://www.instagram.com/p/';

    public function topAll()
    {
        $winner = DB::table('winner')
            ->join('post', 'winner.post_id', '=', 'post.id')
            ->join('account', 'winner.account_id', '=', 'account.id')
            ->join('new_post', 'post.new_post_id', '=', 'new_post.id')
            ->select('new_post.code', 'post.caption', 'post.image_url', 'account.username', 'account.fullname', 'account.profilePicUrl')
            ->orderBy('new_post.created_at', 'decs')
            ->take(10)
            ->get();

        foreach ($winner as $item){
            $item->caption = mb_strimwidth($item->caption, 0, 50, "...");
        }

        return response()->json($winner);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userTop(Request $request)
    {
        try{
            $arr = $request->input('arr');
        } catch (\Exception $ex){
            return response()->json(null);
        }

        $res = [];

        if (empty($arr)){
            return response()->json(null);
        }

        foreach ($arr as $code)
        {
            $newPost = NewPost::where('code', $code)->first();
            $post = Post::where('new_post_id', $newPost->id)->first();
            if (empty($post)){
                continue;
            }

            $winner = Winner::where('post_id', $post->id)->first();

            if (empty($winner)){
                continue;
            }

            $account = Account::where('id', $winner->account_id)->first();

            $shortCaption = mb_strimwidth($post->caption, 0, 20, "...");

            $res[] = [
                'status' => 'found',
                'code' => $code,
                'username' => $account->username,
                'fullname' => $account->fullname,
                'profilePicUrl' => $account->profilePicUrl,
                'caption' => $shortCaption,
                'image_url' => $post->image_url
            ];
        }

        return response()->json($res);
    }

    /**
     * For polling
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function info($id)
    {
        $post = NewPost::where('id', $id)->first();
        if (empty($post)){
            return response()->json(['status' => 'error']);
        }
        if ($post->is_error === 1) {
            switch ($post->status){
                case 4:
                    return response()->json(['status' => 'nocomment']);
                case 6:
                    return response()->json(['status' => 'overlimit']);
                default:
                    return response()->json(['status' => 'error']);
            }
        }

        $accountId = $this->getWinnerAccountId($id);
        if($accountId === 0)
        {
            if ($post->status === 5) {
                return response()->json(['status' => 'longwait']);
            }
            return response()->json(['status' => 'not_found']);
        }
        $account = Account::where('id', $accountId)->first();
        $code = $this->GetCode($id);
        if (empty($code)) {
            return response()->json(['status' => 'error']);
        }

        return response()->json([
            'status' => 'found',
            'title' => 'Winner',
            'username' => $account->username,
            'fullname' => $account->fullname,
            'profilePicUrl' => $account->profilePicUrl,
            'code' => $code
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validator(Request $request)
    {
        //https://www.instagram.com/p/BlxfX-4hZZ6/?utm_source=ig_share_sheet&igshid=by5oya69d01z
        $url = $request->input('url');
        $validUntil = $request->input('valid_until');
        $get_comments = $request->input('get_comments');
        $ts = $request->input('timestamp');
        $type = $request->input('type');
        if (empty($url) || empty($validUntil)) {
            return response()->json(['status' => 'error', 'message' => 'Not correct data']);
        }
        if (empty($ts) && !is_int($ts)) {
            return response()->json(['status' => 'error', 'message' => 'Not correct data']);
        }
        if(empty($type)){
            $type = 0;
        }
        // проверить url на правильный формат дать
        if (!empty($url) && strlen($url) > 37) {
            $pos = strripos($url, self::PostBaseUrl);
            if ($pos === false) {
                return response()->json(['status' => 'error', 'message' => 'Not correct url']);
            } else {
                //добавляем url в базу данных
                $code = $this->GetInstagramPostCode($url);
                $post = NewPost::where('code', $code)->first();
                if (empty($post)) {
                    $newPost = new NewPost();
                    $newPost->code = $code;
                    $newPost->valid_until = (int)$ts;
                    $newPost->is_sync = 0;
                    $newPost->is_error = 0;
                    $newPost->type = $type;
                    $newPost->save();
                    return response()->json(['status' => 'ok', 'message' => 'Пост добавлен', 'id' => $newPost->id]);
                }
                return response()->json(['status' => 'found', 'message' => 'Пост уже был добавлен', 'id' => $post->id]);
            }
        }
        // есть ошибка
        return response()->json(['status' => 'error', 'message' => 'Не корректная ссылка']);
    }

    /**
     * @param $id
     * @return int
     */
    private function getWinnerAccountId($id): int
    {
        $post = Post::where('new_post_id', $id)->first();
        if (empty($post))
            return 0;
        $postId = $post->id;
        $winner = Winner::where('post_id', $postId)->first();
        if (empty($winner))
            return 0;
        return $winner->account_id;
    }

    /**
     * @param string $url
     * @return string
     */
    private function GetInstagramPostCode($url): string
    {
        // 'BlxfX-4hZZ6'
        $str = str_replace(self::PostBaseUrl, "", $url);
        $arr = str_split($str);
        $arr2 = [];
        foreach ($arr as $item) {
            if ($item != '/') {
                $arr2[] = $item;
            } else {
                break;
            }
        }
        $comma_separated = implode($arr2);
        return $comma_separated;
    }
}
