<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string code
 * @property int valid_until
 * @property int is_sync
 * @property int is_error
 * @property int type
 * @property int status
 */
class NewPost extends Model
{
    protected $table = 'new_post';
}
