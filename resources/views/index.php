<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column" id="app">
    <header class="masthead mb-auto">
        <div class="inner">
            <h4 class="masthead-brand" v-show="isWinnersLink"><a href="/contests">Ваши конкурсы</a></h4>
            <nav class="nav nav-masthead justify-content-center">
                <a class="nav-link active" href="/">Главная</a>
                <a class="nav-link" href="/donate">Помочь проекту!</a>
            </nav>
        </div>
    </header>
    <main role="main" class="inner cover" >
        <h1 class="cover-heading">Добавить конкурс</h1>
        <p class="lead">Введите ссылку на пост с конкурсом</p>
        <form action="/validator" method="post" @submit.prevent="checkForm">
            <div class="input-group mb-3">
                <input type="text" class="form-control"
                       placeholder=""
                       aria-label="Введите ссылку на пост"
                       aria-describedby="button-go" formmethod="post" name="url" id="url" v-model="url">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit" id="button-go">Go</button>
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="valid_until">Дата окончание конкурса</label>

                <input type="date" id="valid_until" name="valid_until" max="3000-12-31"
                       min="1000-01-01" class="form-control" formmethod="post"
                       aria-describedby="button-go" v-model="valid_until">
            </div>
            <div class="form-group mb-3">
                <label for="contestsType">Тип конкурса</label>
                <select class="form-control" id="contestsType" v-model="selectedType">
                    <option v-for="option in options" v-bind:value="option.value">
                        {{ option.text }}
                    </option>
                </select>
            </div>
            <div class="form-group mb-3">
                <input type="checkbox" class="form-check-input" id="getComments" name="get_comments" formmethod="post" v-model="getComments">
                <label class="form-check-label" for="getComments">Получить комментарии</label>
            </div>
            <div class="form-group mb-3">
                <p class="lead" v-show="!isUrl" v-for="error in errors">{{ error }}</p>
                <p class="lead" v-show="isDonateUrl">Ограничение в 10 тыс. комментариев. Для того, что бы сервис позволял обрабатывать больше комментариев <a href="https://getwinner.info/donate">Помогите проекту!</a></p>
                <p class="lead" v-show="isUrl"><a v-bind:href="link" target="_blank">Победитель!</a></p>
                <p class="lead" v-show="getComments && isUrl"><a v-bind:href="csvLink" target="_blank">CSV файл</a></p>
            </div>
        </form>
    </main>
    <footer class="mastfoot mt-auto">
        <div class="inner">
            <p>
                Service for <a href="https://www.instagram.com/">Instagram</a>, by <a href="mailto:support@aitool.net">support@aitool.net</a>.&nbsp;
                Service status: {{status}}
            </p>
        </div>
    </footer>
</div>
<script src="./js/jquery.slim.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./build/main.js"></script>
