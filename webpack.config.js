const path = require('path');
const webpack = require('webpack');

const webpackConfig = {
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js',
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  entry: {
    main: './src/index.js', 
    winner: './src/page.js',
    contests: './src/contests.js',
    top: './src/top.js',
    pay: './src/pay.js'
  },
  output: {
    path: path.join(__dirname, 'public/build'),
    publicPath: '/public/build/',
    filename: '[name].js',
  }
};

if (webpackConfig.mode == 'development'){
  webpackConfig.devtool = '#inline-source-map';
}

if (webpackConfig.mode == 'production'){
  webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = webpackConfig;
