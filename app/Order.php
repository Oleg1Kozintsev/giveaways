<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string code
 * @property bool paid
 */
class Order extends Model
{
    protected $table = 'order';
}
