import Vue from "vue";
import axios from 'axios'

Vue.config.productionTip = false;

// register the grid component
Vue.component('winners-grid', {
  template: '#winners-grid',
  props: {
    data: Array,
    columns: Array,
    filterKey: String
  },
  data: function () {
    let sortOrders = {};
    return {
      sortKey: '',
      sortOrders: sortOrders
    }
  },
  computed: {
    filteredData: function () {
      const sortKey = this.sortKey;
      let filterKey = this.filterKey && this.filterKey.toLowerCase();
      let order = this.sortOrders[sortKey] || 1;
      let data = this.data;
      if (filterKey) {
        data = data.filter(function (row) {
          return Object.keys(row).some(function (key) {
            return String(row[key]).toLowerCase().indexOf(filterKey) > -1
          })
        })
      }
      if (sortKey) {
        data = data.slice().sort(function (a, b) {
          a = a[sortKey];
          b = b[sortKey];
          return (a === b ? 0 : a > b ? 1 : -1) * order
        })
      }
      return data
    }
  },
  filters: {
    capitalize: function (str) {
      return str.charAt(0).toUpperCase() + str.slice(1)
    }
  },
  methods: {
    sortBy: function (key) {
      this.sortKey = key;
      this.sortOrders[key] = this.sortOrders[key] * -1;
    }
  }
});

// bootstrap the demo
new Vue({
  el: '#app',
  data: {
    searchQuery: '',
    gridColumns: ['id', 'username'],
    gridData: []
  },
  mounted: function () {
    let self = this;
    let localData = localStorage["getwinner"];
    if(localData === null || localData === undefined){
      return false;
    }
    let arr;
    try{
      arr = JSON.parse(localStorage.getItem("getwinner"));
    } catch(e) {
      console.log(e);
      return false;
    }
    if (!Array.isArray(arr)){
      return false;
    }

    //console.log(arr);

    axios.post('/user_top', {
      arr: arr,
    })
      .then(function (response) {
        //console.log(response);
        self.gridData = response.data;
      })
      .catch(function (error) {
        console.log(error);
    });
  }
});
