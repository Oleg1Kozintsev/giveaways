<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\NewPost;

class Controller extends BaseController
{
    /**
     * @param int $id
     * @return string|null instagram code
     */
    protected function GetCode($id)
    {
        $post = NewPost::where('id', $id)->first();
        if (empty($post))
            return null;
        return $post->code;
    }
}
