<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Getwinner.info - Сервис определения победителей розыгрышей в Instagram">
    <meta name="keywords" content="instagram, drawing, giveawation, smm, marketing, розыгрыши, результаты, конкурсы в инстаграм, giveaway">
    <title><?php echo $title; ?></title>
    <link href="<?=$bootstrapCss; ?>" rel="stylesheet">
    <link href="<?= $pageCss; ?>" rel="stylesheet">
</head>

<body class="text-center">

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
        <div class="inner" id="app2">
            <h4 class="masthead-brand"></h4>
            <nav class="nav nav-masthead justify-content-center">
                <a class="nav-link" href="/">Главная</a>
                <a class="nav-link active" href="/donate">Помочь проекту!</a>
            </nav>
        </div>
    </header>
    <main role="main" class="inner cover">
        <h1>Пожертвуй на развитие проекта!</h1>
        <p class="lead">При разработке данного сервера наша команда тратит большое количество времени и большое
            количество денег
            на оборудование, хостинг и программное обеспечение. Если тебе нравится то, что мы делаем — стань нашим
            другом и пожертвуй небольшую сумму.</p>
        <p class="lead">
            <script language=JavaScript src='https://auth.robokassa.ru/Merchant/PaymentForm/FormMS.js?MrchLogin=<?= $mrh_login ?>&OutSum=<?= $out_summ ?>&InvId=<?= $inv_id ?>&IncCurrLabel=<?= $in_curr ?>&Desc=<?= $inv_desc ?>&SignatureValue=<?= $crc ?>&Shp_item=<?= $shp_item ?>&Culture=<?= $culture ?>&Encoding=<?= $encoding ?>'>
            </script>
        </p>

    </main>
    <footer class="mastfoot mt-auto">
        <div class="inner">
            <p>Service for <a href="https://www.instagram.com/">Instagram</a>, by <a href="https://aitool.net">support@aitool.net</a>.
            </p>
        </div>
    </footer>
</div>
<script src="<?= $jQueryJs; ?>"></script>
<script src="<?= $bootstrapJs; ?>"></script>
</body>
</html>
