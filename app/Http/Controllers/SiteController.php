<?php

namespace App\Http\Controllers;

use App\NewPost;
use App\Order;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    private const mrh_pass1 = "dS4KOL7bafL15icpdJ0X";
    // test
    //private const mrh_pass1 = "BYp3rgj5uX7XWVNZ2Xy0";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('layout', [
            'title' => 'Getwinner.info - Сервис определения победителей розыгрышей в Instagram | Выгрузка комментариев из Инстаграм',
            'context' => view('index')
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function post($id)
    {
        $code = $this->GetCode($id);
        if (empty($code)) {
            return view('not_found');
        }
        return view('post', [
            'title' => 'Победитель | Getwinner.info - Сервис определения победителей розыгрышей в Instagram'
        ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function contests()
    {
        return view('contests', [
            'title' => 'Ваши конкурсы | Getwinner.info - Сервис определения победителей розыгрышей в Instagram',
        ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function donate()
    {
        return view('layout', [
            'title' => 'Пожертвуйте на развитие проекта! | Getwinner.info - Сервис определения победителей розыгрышей в Instagram',
            'context' => view('donate')
        ]);
    }

    public function topAll()
    {
        return view('top', [
            'title' => 'Top 10 | Getwinner.info - Сервис определения победителей розыгрышей в Instagram'
        ]);
    }

    public function notFound()
    {
        return view('not_found');
    }

    public function pay($code)
    {
        $mrh_login = "getwinner";
        $mrh_pass1 = self::mrh_pass1;

        $find = Order::where('code', $code)->first();

        if (empty($find)) {
            $order = new Order();
            $order->code = $code;
            $order->save();
            $inv_id = $order->id;
        } else {
            $inv_id = $find->id;
        }

        $out_summ = "19.00";

        $shp_item = 1;

        $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");

        $crc = "948087697cbe2f68ddc929dff7daa3ca";


        return view('pay', [
            'title' => 'Пожертвуйте на развитие проекта! | Getwinner.info - Сервис определения победителей розыгрышей в Instagram',
            'mrh_login' => $mrh_login,
            'inv_id' => $inv_id,
            'inv_desc' => 'ROBOKASSA Advanced User Guide',
            'out_summ' => $out_summ,
            'shp_item' => $shp_item,
            'in_curr' => '',
            'culture' => 'ru',
            'encoding' => 'utf-8',
            'crc' => $crc,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function paySuccess(Request $request)
    {
        $OutSum = $request->input('OutSum');
        $InvId = $request->input('InvId');
        $Shp_item  = $request->input('Shp_item');
        $SignatureValue  = $request->input('SignatureValue');

        $mrh_pass1 = self::mrh_pass1;

        $crc = strtoupper($SignatureValue);

        $my_crc = strtoupper(md5("$OutSum:$InvId:$mrh_pass1:Shp_item=$Shp_item"));

        // проверка корректности подписи
        // check signature
        if ($my_crc != $crc)
        {
            return view('layout', [
                'title' => 'Getwinner.info - Сервис определения победителей розыгрышей в Instagram',
                'context' => view('pay_fail', [
                    'info' => 'bad sign'
                ])
            ]);
        }

        // проверка наличия номера счета в истории операций
        // check of number of the order info in history of operations
        $order = Order::where('id', $InvId)->first();
        if (empty($order)) {
            //todo:
            return view('layout', [
                'title' => 'Getwinner.info - Сервис определения победителей розыгрышей в Instagram',
                'context' => view('pay_fail', [
                    'info' => 'order not found'
                ])
            ]);
        }
        $code = $order->code;
        $order->paid = 1;
        $order->save();

        $newPost = NewPost::where('code', $code)->first();
        if (empty($newPost)) {
            return view('layout', [
                'title' => 'Getwinner.info - Сервис определения победителей розыгрышей в Instagram',
                'context' => view('pay_fail', [
                    'info' => 'new post not found'
                ])
            ]);
        }
        $newPost->paid = 1;
        $newPost->save();
        $postId = $newPost->id;

        // к страницы с получением резултата
        return view('layout', [
            'title' => 'Getwinner.info - Сервис определения победителей розыгрышей в Instagram',
            'context' => view('pay_success', [
                'postId' => $postId
            ])
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function payFail(Request $request)
    {
        $InvId = $request->input('InvId');
        return view('layout', [
            'title' => 'Getwinner.info - Сервис определения победителей розыгрышей в Instagram',
            'context' => view('pay_fail', [
                'info' => 'new post not found'
            ])
        ]);
    }
}
