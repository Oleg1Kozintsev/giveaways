<script type="text/html" id="postId">
    <?= $postId; ?>
</script>

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
        <div class="inner" id="app2">
            <h4 class="masthead-brand"></h4>
            <nav class="nav nav-masthead justify-content-center">
                <a class="nav-link" href="/">Главная</a>
                <a class="nav-link active" href="/donate">Помочь проекту!</a>
            </nav>
        </div>
    </header>
    <main role="main" class="inner cover" id="app">
        <h1>Платёж принят</h1>
        <p class="lead">После того, как сервис обработает данные, победитель будет доступен по ссылке:</p>
        <p class="lead" v-show="url"><a v-bind:href="url" target="_blank">Победитель!</a></p>
        <p class="lead">Топ 10 победителей доступно по ссылке:</p>
        <p class="lead"><a href="https://getwinner.info/top10" target="_blank">TOP 10</a></p>
    </main>
    <footer class="mastfoot mt-auto">
        <div class="inner">
            <p>Service for <a href="https://www.instagram.com/">Instagram</a>, by <a href="https://aitool.net">support@aitool.net</a>.
            </p>
        </div>
    </footer>
</div>
<script src="<?= $jQueryJs; ?>"></script>
<script src="<?= $bootstrapJs; ?>"></script>
<script src="<?= $PayJs; ?>"></script>

