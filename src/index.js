import Vue from 'vue';
//import VueRouter from 'vue-router';
import axios from 'axios'
import _ from 'lodash'

function getTimeStamp(input) {
  let parts = input.trim().split(' ');
  let date = parts[0].split('-');
  let time = (parts[1] ? parts[1] : '00:00:00').split(':');

  // NOTE:: Month: 0 = January - 11 = December.
  let d = new Date(date[0], date[1] - 1, date[2], time[0], time[1], time[2]);
  return d.getTime() / 1000;
}

function today() {
  const now = new Date();
  return now.getFullYear() + '-' + ('0' + (now.getMonth() + 1)).slice(-2) + '-' + ('0' + now.getDate()).slice(-2);
}

function getInstagramPostCode(url) {
  if (url === null || url === undefined) {
    return null;
  }
  let str = _.replace(url, "https://www.instagram.com/p/", "");
  let arr = str.split('');
  let arr2 = [];
  let c = arr.length;
  for (let i = 0; i < c; i++) {
    if (arr[i] !== '/') {
      arr2.push(arr[i]);
    } else {
      break;
    }
  }
  return arr2.join('');
}

Vue.config.productionTip = false;

//const router = new VueRouter() ;

new Vue({
  el: '#app',
  data: {
    errors: [],
    url: null,
    valid_until: today(),
    link: null,
    postId: null,
    isUrl: false,
    isDonateUrl: false,
    pooling: false,
    selectedType: '0',
    selText: '',
    options: [{
      text: 'Случайный выбор комментария',
      value: '0'
    }, {
      text: 'Нужно указать двух и более друзей',
      value: '1'
    }, {
      text: 'Нужно указать трёх и более друзей',
      value: '2'
    }],
    status: 'not working',
    getComments: false,
    csvLink: null
  },
  computed: {
    timestamp: function () {
      // необходимо парсить, потому что Vue
      // преобразует пустое значение в строку
      return getTimeStamp(this.valid_until + ' 23:59:59')
    },
    isWinnersLink: function () {
      let localData = localStorage["getwinner"];
      if (localData === null || localData === undefined) {
        return false;
      }
      let arr;
      try {
        arr = JSON.parse(localStorage.getItem("getwinner"));
      } catch (e) {
        console.log(e);
        return false;
      }
      return Array.isArray(arr);
    }
  },
  methods: {
    startPolling: function () {
      let self = this;
      axios.get('/info/' + this.postId)
        .then(function (response) {
          // handle success
          //console.log(response);
          //console.log("Status first: " + response.data.status);
          //console.log('pooling: ' + self.pooling);
          if (response.data.status === 'found') {
            //console.log("Status second: " + response.data.status);
            self.pooling = false;
            self.isUrl = true;
          }
          if (response.data.status === 'longwait') {
            self.errors = [];
            self.errors.push("Большое колличество комментариев. Пожалуйста ждите 15-30 минут.");
          }
          if(response.data.status === 'overlimit') {
            self.pooling = false;
            self.errors = [];
            self.errors.push("Слишком много коментариев.");
            self.isDonateUrl = true;
          }
          if(response.data.status === 'nocomment') {
            self.pooling = false;
            self.errors = [];
            self.errors.push("Нет комментариев");
          }
          if(response.data.status === 'error') {
            self.pooling = false;
            self.errors = [];
            self.errors.push("Ошибка при обработке");
          }
        })
        .catch(function (error) {
          // handle error
          //console.log(error);
        })
        .then(function () {
          // always executed
          //console.log('always executed');
          if (self.pooling === true) {
            //console.log('pooling: ' + self.pooling);
            setTimeout(() => {
              self.startPolling();
            }, 3000);
          }
        });
    },
    checkForm: function (e) {
      this.errors = [];
      this.link = null;
      this.postId = null;
      this.pooling = false;
      this.isUrl = false;
      this.isDonateUrl = false;

      if (this.url && this.valid_until) {
        if (this.url.length < 38 || !_.startsWith(this.url, 'https://www.instagram.com/p/')) {
          this.errors.push('Не верный формат ссылки. Пример ссылки:');
          this.errors.push('https://www.instagram.com/p/BmLV16NBP91/?taken-by=buzova86');
          return false;
        }
        let self = this;

        axios.post('/validator', {
          url: this.url,
          valid_until: this.valid_until,
          timestamp: this.timestamp,
          type: this.selectedType,
          get_comments: this.getComments
        })
          .then(function (response) {
            //console.log(response);
            self.errors.push(response.data.message);
            if (response.data.status === 'found' || response.data.status === 'error'){
              return;
            }
            self.link = 'post/' + response.data.id;
            self.postId = response.data.id;
            self.pooling = true;
            if (self.getComments){
              self.csvLink = 'export_comments_id/' + response.data.id;
            }
            self.errors.push("Пожалуйста ждите ...");
            // записываем в стор code поста, для списка победителей
            let code = getInstagramPostCode(self.url); // нужно получить код из url
            let localData = localStorage["getwinner"];
            let arr = [];

            if (localData !== null || localData !== undefined) {
              let store = localStorage.getItem("getwinner");
              if (store !== null) {
                arr = JSON.parse(store);
                // не больее 11 элементов в localStore
                if (arr.length > 10) {
                  arr.shift();
                }
              }
            }

            arr.push(code);

            localStorage.setItem("getwinner", JSON.stringify(arr)); //запишем его в хранилище по ключу "myKey"

            self.url = '';

            if (self.status === 'not working'){
              self.errors = [];
              self.errors.push("Сервис недоступен. Пожалуйста, обратитесь к администрации сайта.");
              return;
            }
            // todo: редирект на оплату
            //router.push(`/pay/${code}`);
            window.location.replace(`https://getwinner.info/pay/${code}`);

            // setTimeout(() => {
            //   self.startPolling()
            // }, 3000);
          })
          .catch(function (error) {
            //console.log(error);
            self.errors.push(error);
          });

        return true;
      }

      if (!this.url) {
        this.errors.push('Требуется указать ссылку на пост');
      }
      if (!this.valid_until) {
        this.errors.push('Требуется указать дату окончания конкурса');
      }

      e.preventDefault();
    }
  },
  mounted: function () {
    let self = this;
    axios.get("https://getwinner.info:1306/status")
      .then(function (response) {
        //console.log(response);
        if(response.status === 200){
          self.status = "working";
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }
});
