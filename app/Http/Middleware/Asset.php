<?php
namespace App\Http\Middleware;

use Closure;

class Asset
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $urlGenerator = app('url');
        $bootstrapCss = $urlGenerator->asset('/css/bootstrap.min.css');
        $coverCss = $urlGenerator->asset('/css/cover.min.css');
        $pageCss = $urlGenerator->asset('/css/page.min.css');
        $bootstrapJs = $urlGenerator->asset('/js/bootstrap.min.js');
        $jQueryJs = $urlGenerator->asset('/js/jquery.slim.min.js');
        $PageJs = $urlGenerator->asset('/build/winner.js');
        $WinnerListJs = $urlGenerator->asset('/build/contests.js');
        $PageNotFoundCss = $urlGenerator->asset('/css/404.min.css');
        $TopJs = $urlGenerator->asset('/build/top.js');
        $PayJs = $urlGenerator->asset('/build/pay.js');
        view()->share('bootstrapCss', $bootstrapCss);
        view()->share('coverCss', $coverCss);
        view()->share('pageCss', $pageCss);
        view()->share('bootstrapJs', $bootstrapJs);
        view()->share('jQueryJs', $jQueryJs);
        view()->share('PageJs', $PageJs);
        view()->share('WinnerListJs', $WinnerListJs);
        view()->share('PageNotFoundCss', $PageNotFoundCss);
        view()->share('TopJs', $TopJs);
        view()->share('PayJs', $PayJs);
        return $next($request);
    }
}
