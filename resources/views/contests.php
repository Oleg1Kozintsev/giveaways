<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Getwinner.info - Сервис определения победителей розыгрышей в Instagram">
	<meta name="keywords" content="instagram, drawing, giveawation, smm, marketing, розыгрыши, результаты, конкурсы в инстаграм, giveaway">
    <title><?php echo $title; ?></title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/top.min.css" rel="stylesheet">
</head>
<body class="text-center">
<!-- component template -->
<script type="text/x-template" id="winners-grid">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">
                Конкурс
            </th>
            <th scope="col">
                Победитель
            </th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="entry in filteredData">
            <td>
                <div class="media p-3">
                    <a v-bind:href="'https://www.instagram.com/p/' + entry['code']" target="_blank">
                        <img v-bind:src="entry['image_url']" v-bind:alt="entry['caption']"
                             class="mr-3 mt-3 rounded-circle"
                             style="width:60px;">
                        <div class="media-body">
                            <p>{{entry['caption']}}</p>
                        </div>
                    </a>
                </div>
            </td>
            <td>
                <div class="media p-3">
                    <a v-bind:href="'https://www.instagram.com/' + entry['username']" target="_blank">
                        <img v-bind:src="entry['profilePicUrl']" v-bind:alt="entry['username']"
                             class="mr-3 mt-3 rounded-circle" style="width:60px;">
                        <div class="media-body">
                            <p>{{ entry['fullname'] ? entry['fullname'] : entry['username'] }}</p>
                        </div>
                    </a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</script>

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
        <div class="inner" id="app2">
            <h4 class="masthead-brand"><a href="/contests">Ваши конкурсы</a></h4>
            <nav class="nav nav-masthead justify-content-center">
                <a class="nav-link" href="/">Главная</a>
                <a class="nav-link" href="/donate">Помочь проекту!</a>
            </nav>
        </div>
    </header>
    <main role="main" class="inner cover text-left" id="app">
        <h1 class="cover-heading">Список победителей</h1>
        <div class="table-responsive">

            <winners-grid
                    :data="gridData"
                    :columns="gridColumns"
                    :filter-key="searchQuery">
            </winners-grid>
        </div>
    </main>
    <footer class="mastfoot mt-auto">
        <div class="inner">
            <p>Service for <a href="https://www.instagram.com/">Instagram</a>, by <a href="mailto:support@aitool.net">support@aitool.net</a>.
            </p>
        </div>
    </footer>
</div>
<script src="<?php echo $jQueryJs; ?>"></script>
<script src="<?php echo $bootstrapJs; ?>"></script>
<script src="<?php echo $WinnerListJs; ?>"></script>
</body>
</html>
