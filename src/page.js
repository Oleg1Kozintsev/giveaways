import Vue from 'vue'
import axios from 'axios'

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  data: {
    errors: [],
    profilePicUrl: null,
    username: null,
    fullname: null,
    pooling: true,
    code: null
  },
  computed: {
    userUrl: function () {
      // необходимо парсить, потому что Vue
      // преобразует пустое значение в строку
      return "https://www.instagram.com/" + this.username;
    },
    postUrl: function () {
      // необходимо парсить, потому что Vue
      // преобразует пустое значение в строку
      return "https://www.instagram.com/p/" + this.code;
    },
    postId: function () {
      const path = location.pathname;
      const parts = path.split('/');
      const param = parts[parts.length - 1];
      return param;
    }
  },
  mounted: function () {
    this.startPolling();
  },
  methods: {
    startPolling: function () {
      let self = this;
      axios.get('/info/' + this.postId)
        .then(function (response) {
          // handle success
          //console.log(response);
          if (response.data.status === 'found') {
            self.pooling = false;
            self.username = response.data.username;
            self.fullname = response.data.fullname;
            self.profilePicUrl = response.data.profilePicUrl;
            self.code = response.data.code;
            // показать форму
          }
        })
        .catch(function (error) {
          // handle error
          //console.log(error);
        })
        .then(function () {
          // always executed
          //console.log('always executed');
          if (self.pooling === true) {
            console.log('pooling');
            setTimeout(() => {
              self.startPolling();
            }, 3000);
          }
        });
    },
  }
});
