<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Getwinner.info - Сервис определения победителей розыгрышей в Instagram">
	<meta name="keywords" content="instagram, drawing, giveawation, smm, marketing, розыгрыши, результаты, конкурсы в инстаграм, giveaway">
    <title>Page not found | Getwinner.info - Сервис определения победителей розыгрышей в Instagram</title>
    <link href="<?= $PageNotFoundCss ?>" rel="stylesheet">
</head>
<body>
<div class="text-wrapper">
    <div class="title" data-content="404">
        404
    </div>

    <div class="subtitle">
        Oops, the page you're looking for doesn't exist.
    </div>

    <div class="buttons">
        <a class="button" href="/">Go to homepage</a>
    </div>
</div>
</body>
</html>
