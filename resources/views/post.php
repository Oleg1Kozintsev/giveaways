<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Getwinner.info - Сервис определения победителей розыгрышей в Instagram">
	<meta name="keywords" content="instagram, drawing, giveawation, smm, marketing, розыгрыши, результаты, конкурсы в инстаграм, giveaway">
    <title><?php echo $title; ?></title>
    <link href="<?=$bootstrapCss; ?>" rel="stylesheet">
    <link href="<?= $pageCss; ?>" rel="stylesheet">
</head>

<body class="text-center">
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column" id="app">
    <header class="masthead mb-auto">
        <div class="inner">
            <h3 v-if="pooling" class="masthead-brand">Победитель ещё не определён.</h3>
            <h3 v-else class="masthead-brand">Победитель!</h3>
            <nav class="nav nav-masthead justify-content-center">
                <a class="nav-link" href="/">Главная</a>
                <a class="nav-link" href="/donate">Помочь проекту!</a>
            </nav>
        </div>
    </header>

    <main role="main" class="inner cover">
        <div v-show="pooling">
            <h5>Пожалуйста ожидайте...</h5>
        </div>
        <div v-show="!pooling" class="media">
            <img class="mr-3" v-bind:src="profilePicUrl" v-bind:alt="fullname">
            <div class="media-body" style="text-align: left;">
                <h5 class="mt-0">{{fullname}}</h5>
                <p><a v-bind:href="userUrl" target="_blank">{{username}}</a></p>
                <p><a v-bind:href="postUrl" target="_blank">Ссылка на сообщение с конкурсом</a></p>
            </div>
        </div>
    </main>

    <footer class="mastfoot mt-auto">
        <div class="inner">
        <p>Service for <a href="https://www.instagram.com/">Instagram</a>, by <a href="mailto:support@aitool.net">support@aitool.net</a>.
        </div>
    </footer>
</div>
<script src="<?= $jQueryJs; ?>"></script>
<script src="<?= $bootstrapJs; ?>"></script>
<script src="<?= $PageJs; ?>"></script>
</body>
</html>
