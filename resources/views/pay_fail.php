<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
        <div class="inner" id="app2">
            <h4 class="masthead-brand"></h4>
            <nav class="nav nav-masthead justify-content-center">
                <a class="nav-link" href="/">Главная</a>
                <a class="nav-link active" href="/donate">Помочь проекту!</a>
            </nav>
        </div>
    </header>
    <main role="main" class="inner cover">
        <h1>Платёж отменён</h1>
        <p><?php echo $info; ?></p>
    </main>
    <footer class="mastfoot mt-auto">
        <div class="inner">
            <p>Service for <a href="https://www.instagram.com/">Instagram</a>, by <a href="https://aitool.net">support@aitool.net</a>.
            </p>
        </div>
    </footer>
</div>
<script src="<?= $jQueryJs; ?>"></script>
<script src="<?= $bootstrapJs; ?>"></script>
