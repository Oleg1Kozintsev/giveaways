<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int post_id
 * @property int account_id
 */

class Winner extends Model
{
    protected $table = 'winner';

}