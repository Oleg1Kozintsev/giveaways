<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', 'SiteController@index');

$router->get('post/{id}', 'SiteController@post');

$router->get('donate', 'SiteController@donate');

$router->get('contests', 'SiteController@contests');

$router->get('top10', 'SiteController@topAll');

$router->get('not_found', 'SiteController@notFound');

$router->get('pay/{code}', 'SiteController@pay');

$router->get('pay_success', 'SiteController@paySuccess');

$router->get('pay_fail', 'SiteController@payFail');

$router->post('validator', 'ApiController@validator');

$router->get('info/{id}', 'ApiController@info');

$router->post('user_top', 'ApiController@userTop');

$router->get('top_all', 'ApiController@topAll');

$router->get('export_comments/{code}', 'CsvController@exportCommentsByCode');

$router->get('export_comments_id/{id}', 'CsvController@exportCommentsById');
