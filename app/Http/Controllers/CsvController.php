<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CsvController extends Controller
{
    /**
     * @param int $id
     * @return mixed
     */
    public function exportCommentsById($id)
    {
        $comments = DB::table('comments')
            ->join('account', 'comments.account_id', '=', 'account.id')
            ->join('post', 'comments.post_id', '=', 'post.id')
            ->join('new_post', 'post.new_post_id', '=', 'new_post.id')
            ->select('account.username', 'account.fullname', 'comments.caption')
            ->where('new_post.id', $id)
            ->get();

        if ($comments->isEmpty()) {
            return view('not_found');
        }

        return $this->createCsvFileResponse($comments);
    }

    /**
     * @param string $code
     * @return mixed
     */
    public function exportCommentsByCode($code)
    {
        $comments = DB::table('comments')
            ->join('account', 'comments.account_id', '=', 'account.id')
            ->join('post', 'comments.post_id', '=', 'post.id')
            ->join('new_post', 'post.new_post_id', '=', 'new_post.id')
            ->select('account.username', 'account.fullname', 'comments.caption')
            ->where('new_post.code', $code)
            ->get();

        if ($comments->isEmpty()) {
            return view('not_found');
        }

        return $this->createCsvFileResponse($comments);
    }

    private function createCsvFileResponse($comments)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = ['Username', 'Fullname', 'Caption'];

        ob_start();

        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($comments as $comment) {
            fputcsv($file, [
                $comment->username,
                $comment->fullname,
                $comment->caption,

            ]);
        }

        $value = ob_get_contents();
        ob_end_clean();
        fclose($file);

        return (new Response($value, 200))->withHeaders($headers);
    }
}
