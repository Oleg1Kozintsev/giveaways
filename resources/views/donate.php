<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
        <div class="inner" id="app2">
            <h4 class="masthead-brand"></h4>
            <nav class="nav nav-masthead justify-content-center">
                <a class="nav-link" href="/">Главная</a>
                <a class="nav-link active" href="/donate">Помочь проекту!</a>
            </nav>
        </div>
    </header>
    <main role="main" class="inner cover">
        <h1>Пожертвуй на развитие проекта!</h1>
        <p class="lead">При разработке данного сервера наша команда тратит большое количество времени и большое
            количество денег
            на оборудование, хостинг и программное обеспечение. Если тебе нравится то, что мы делаем — стань нашим
            другом и пожертвуй любую сумму.</p>
        <p class="lead">Самый удобный способ для пользователей СНГ — через шлюз Яндекс.Денег с банковской карточки или
            Яндекс.Кошелька.</p>
        <a class="shortcode-this" href="https://money.yandex.ru/to/410015409987387" target="_blank">Жми!
            <p><span class="shortcode-this-icon">&darr;</span></p>
            <img class="shortcode-this-image" style="background-color: white" src="./img/ya-money-logo.gif"
                 alt="Яндекс.Деньги логотип">
        </a>
        <p class="lead">Так же вы можете пожертвовать переводом на карту Сбербанка:</p>
        <div class="input-group mb-3">
            <input type="text" class="form-control"
                   placeholder="" id="copyTarget" value="4276 6900 1433 7353" readonly>
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit" id="copyButton">Copy</button>
            </div>
        </div>
    </main>
    <footer class="mastfoot mt-auto">
        <div class="inner">
            <p>Service for <a href="https://www.instagram.com/">Instagram</a>, by <a href="mailto:support@aitool.net">support@aitool.net</a>.
            </p>
        </div>
    </footer>
</div>
<script src="<?= $jQueryJs; ?>"></script>
<script src="<?= $bootstrapJs; ?>"></script>
<script>
    document.getElementById("copyButton").addEventListener("click", function() {
        copyToClipboard(document.getElementById("copyTarget"));
    });


    function copyToClipboard(elem) {
        let target;
        // create hidden text element, if it doesn't already exist
        const targetId = "_hiddenCopyText_";
        const isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        let origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        const currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        let succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        return succeed;
    }
</script>
